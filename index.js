let http = require("http");
const port = 4000


http.createServer(function(request,response){

// HTTP Routing Methods: Get, Post, Put, Delete
	if(request.url == "/," && request.method == "GET"){

		// HTTP Method of the incoming request can be accessed via the method property of the request parameter
		// The method "GET" means that we will be retrieving or reading an information
		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end("Welcome to Booking System")
	}	

	else if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end('Welcome to your profile!')
	}

	else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end('Here’s our courses available!')
	}

	else if(request.url == "/addcourse" && request.method == "GET"){
		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end('Add a course to our resources!')
	}

}).listen(4000);

console.log("Server running at localhost:4000");